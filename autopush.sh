#!/bin/bash

#--------------------------------------------
# 这是一个自动git commit的脚本

# 功能：自动进行git commit
#
# 使用：将脚本git工程目录下面，
#       输入./autocommit.sh your commit

# 依赖：shell
#--------------------------------------------


echo "-------Begin-------"
git add .
git commit -m "$*"
echo "$*"
git push -u origin master
echo "--------End--------"
